# Quarkus (Java) Application CI - CD with Bitbucket pipeline

## Introduction

This is a simple hello world with Quarkus (Java) which contains all the bitbucket-pipeline.yml steps for Continuous integration and Continuous Deployment using AWS ECR to store docker images and ec2 to deploy it.

## Prerquesite

### AWS

#### IAM

You need first to configure a group and a user with ECS/EC2 and get the **AWS_SECRET_ACCESS_KEY** and **AWS_ACCESS_KEY**.

If you don't know how to do it you can follow the instructions [here](./documentation/iam)

#### ECR

You need then to create your docker repository to store your images.

If you don't know how to do it you can follow the instructions [here](./documentation/ecr)

#### EC2

To finish you need then to launch an ec2 instance.

If you don't know how to do it you can follow the instructions [here](./documentation/ec2)

### Bitbucket

Now we are done. We have to:

- Configure the enironment variables of our pipeline
- Configure an ssh key pair to access to our ec2 instance

#### Configure variable

To do this go to `Settings/Pipelines/Repository variables` and fill your variable. Be carreful to put at least the AWS_SECRET_ACCESS_KEY as **Secured**!

![Variable](./documentation/bitbucket/BB_Configure_Variables.png)

Here are the variable and their mean:

- APPLICATION_NAME: this will be use as name to build your docker image and it must match your ECR repository name
- AWS_ECR_URL: The url to your ECR repository (used to push and fetch your images)
- AWS_EC2_TEST_SSH_USER: The user to use to connect to ec2 instance
- AWS_EC2_TEST_URL: The url we will target to test if app is running but as well to connect to ec2 instance
- AWS_REGION: The region you selected for you ECR repository
- AWS_ACCESS_KEY: this is the access key id you got once you created the user in iam section (it will be used to connect to AWS ECR)
- AWS_ACCESS_SECRET_KEY: THIS MUST BE KEPT SECRET (use Secured). This is the secret access key id you got once you created the user in iam section (it will be used to connect to AWS ECR)
- PRODUCTION_BRANCH: This define the branch used for production releasing. You can as well use a pattern with variable RELEASE_PATTERN (like RELEASE_PATTERN=release/*). For more information you can check `Release cycle` section)
- INTERNAL_PORT_TARGET: Once you application will be up and running we will target http://localhost:${INTERNAL_PORT_TARGET}${LIVENESS_PATH}
- LIVENESS_PATH: Once you application will be up and running we will target http://localhost:${INTERNAL_PORT_TARGET}${LIVENESS_PATH}

#### Configure ssh key pair

To use an ssh key pair I recommand you to not use the one you created for EC2 but to create one. To do this open a terminal and type `ssh-keygen`. Do not change the name, do not enter a passphrase:

![ssh-keygen](./documentation/other/ssh-keygen.png).

Once done you need to copy the new key in the authorized key of your ec2 instance. You can use the `ssh-copy-id` or you can go on your instance and modify the `~/.ssh/authorized_key` by simply adding a new line with the `id_rsa.pub`.

Once done you need to go to `Settings/Pipelines/SSH keys` and add your private + public key. You need as well to add the host as known host:

![Configure ssh](./documentation/bitbucket/BB_Configure_SSH.png).

We are now ready to run our pipeline

## The pipeline content

### Base image

The current pipeline is using image: `speedflyer/bitbucket-pipeline-java-aws-ssh` you can see the content of the Dockerfile [here](https://hub.docker.com/r/speedflyer/bitbucket-pipeline-java-aws-ssh/dockerfile). This is just an image that extends the openjdk one. This adds as well the the ssh to connect to ec2 instance and aws cli.

### Steps

At the moment it propose for all branches other than `release/*` and `master`:

- Init: check variables
- Build: perform a build and run test

For `release/*` and `master` additionnal steps are done:

- The release-preparation: Release preparation is doing handling the release cycle. To have a clear view of what it is doing you can go to the section **Release cycle**
- The publish: This will publish to your aws ECR repository your image built. It will add the tag Example: `dev-develop-1.0.2-1`
- The deploy-to-target: By specifying the targeted ec2 url your code will be deployed. It will connect to your ec2 instance. Pull the image. Kill and delete the current docker container running your app. Start the docker container with your new image.
- Post execution (only for releases branches): This will put back the `-SNAPSHOT` version

### Release cycle

It is possible to configure the release cycle. 

#### Default release cycle

By default it is configured for one production branch.
For example if you have a `develop` branch for your QA phase and `master` as a production branch you need to specify the variable `PRODUCTION_BRANCH=master`.

What will be done:

- If the branch is not the production branch, the maven version is not updated. But a tag will be created with format `<branch_name>-<maven major minor>.<bitbucket build number>`. Example: `develop-1.0.32`
- If the branch is the production branch, only the patch version will be increased. The maven version will be updated and a tag will be created with the maven version before publishing and deploying. Then the `-SNAPSHOT` version will be re-added (see: `scripts/put-back-snapshot.sh`). If a minor or major change occures. It is up to the developer or release engineer to increase the minor/major manually on the development branch and then push this change to the production branch.

#### Multiple release branches

If working on several release branches it is possible to specify the releasing pattern with variable `RELEASE_PATTERN`. The pattern takes the following assumption:

- Release branches will be formatted with `<prefix><major>.<minor>`. Example: `release/1.0` or `production-2.4`. 
- The pattern must be explained in the following format: `<prefix>*`. Example: `release/*` or `production-*`. The `*` indicates where the version must be taken.

Two possible solutions:

- If the branch is not a release branch, the maven version is not updated. But a tag will be created with format `<branch_name>-<maven major minor>.<bitbucket build number>`. Example: `develop-1.0.32`
- If the branch is a release branch, the RELEASE_PATTERN will be computed. The version from the relase name is taken (example: `1.0`). 
    - If the version matches the `<major>.<minor>` of the maven version then only the patch version will be increased. maven version will be updated and a tag will be created with the maven version before publishing and deploying. Then the `-SNAPSHOT` version will be re-added (see: `scripts/put-back-snapshot.sh`)
    - If it does not match, it means that it is the first build of this release. The maven version will be updated to the current release branch version with the patch `0` (example: `2.4.0` for `release/2.4` matching pattern `release/*`) and a tag will be created with the maven version before publishing and deploying. Then the `-SNAPSHOT` version will be re-added (see: `scripts/put-back-snapshot.sh`).

> If you want to use multiple release branches don't forget to adapt your `bibucket-pipeline.yml` to use 

```
branches:
    release/*:
      - step: *init
      - ...
```

> A special note: if your branch name contains "/" or any other specific characters. They will be replaced by "_" as the tag name will be used to publish

Here is a capture of your pipeline:

![Pipeline](./documentation/bitbucket/BB_Pipeline.png).

Here is an example with Multiple branch releasing:

![Releasing](./documentation/bitbucket/BB_Commit_List.png).


You can follow you deployments in the deployment section. Here is an example: 

![Deployment](./documentation/bitbucket/BB_Deployments.png).

