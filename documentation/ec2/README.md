# EC2

## Prerequesite

- You need first to create an aws account. (if it is not the case go to [this page](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/))
- Then go to [EC2 Home](https://aws.amazon.com/fr/ec2/). EC2 stands for Amazon Elastic Compute Cloud. It's basically a service which gives you the possibility to launch cloud instances. You can choose the OS (linux, Windows etc), the capacity (RAM, CPU, additional disks). You can as well define Security rules and plenty of other things (I let you have a look to the home [EC2 Home](https://aws.amazon.com/fr/ec2/)).

## Create a Security Group

A security group is a kind of virtual firewall. You can configure ingress and egress rules. For our app we won't configure egress rules. But we want for input:

- An access to port 22 from our IP Adress
- An access to port 22 from bitbucket Pipeline
- An access to port 5000 from everywhere

To do this you have to go to the ec2 Dashboard and select in the menu "Security Groups" (In "Network and security" section)

![Create security group](./EC2_Dashboard.png)

Click then on "Create Security Group".

Give it a name and a description. Add as well the rules mentionned above (ignore the Bitbucket pipeline one).

![Create security group](./EC2_CreateSecurityGroup_1.png)

For bitbucket pipeline rules you can either go on the following [site](https://confluence.atlassian.com/bitbucket/what-are-the-bitbucket-cloud-ip-addresses-i-should-use-to-configure-my-corporate-firewall-343343385.html) and enter manually the IPs.

I am pretty lazy guy so I installed the AWS CLI and created a script that configure them manually. You can have a look [here](../../scripts/). The script name is: `configure-ec2-access-bitbucket.sh`.

To use the aws cli you can check how to install it [here](https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/cli-chap-install.html). 

You will then have to configure it. Don't forget that if you want to modify it you need a special user with ec2 access. You can have a look to [iam section](../iam). To know how to create a programmatic user. You will have to give him the "EC2FullAccess" policy.

```
aws configure set region <REGION>
aws configure set output json
aws configure set aws_secret_access_key <AWS_SECRET_ACCESS_KEY>
aws configure set aws_access_key_id <AWS_ACCESS_KEY> 
```

## Launch your instance

You are know ready to launch your instance.

On the ec2 Dashboard select in the menu "Instances" (In "Instances" section)

![Launch instance](./EC2_Dashboard.png)

Then click on "Launch instance"

You first need to select your Image. Select "ubuntu 18.04"

![Launch instance](./EC2_Launch_Instance.png)

You then need to select an instance type. For information a "t2.micro" (with 1CPU and 1 GB of memory is eligible for free tier).

![Launch instance](./EC2_Launch_Instance_1.png)

Click on next and you are on the instance Details. You have plenty of option. I let you have a look and configure what you want. Then click on "Next: Add storage"

On this page You have the storage details. You can add additionnal volumes if you want. Click on "Next: Add Tags".

Here you can configure the tags to identify easily your machine. Once done continue to "Next: Configure Security".

On this page we will assign our security group. Click on "Select an existing security group" and select the one you just created

![Launch instance](./EC2_Launch_Instance_2.png)

Finally click on "Review and Launch" and then after your final review click on "Launch". You will be prompted to select an SSH key pair. You can either select an existing one or create one. If you create one, don't forget to download the key pair.

![Launch instance](./EC2_Launch_Instance_3.png) 

> Your instance has a dynamic IP (it will change each time you restart it). You can assign an Elastic IP. To do this go from the dashboard to "Elastic IPs" in "Network and Security" section. Then click on "Allocate new address". Be carreful if your IP is assigned to a not running instance you will have a charge