# ECR

## Prerequesite

- You need first to create an aws account. (if it is not the case go to [this page](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/))
- Then go to [ECR Home](https://aws.amazon.com/fr/ecr/). ECR stands for Amazon Elastic Container Registry. It's a docker container registry which help you to stock manage and deploy images.

## Create a repository

You need first to create a repository to store your images. 

You need first to click on "Create Repository"

![Create repostitory](./ECR_Create_Repository.png)

Enter a name and save it. You should then see it.

![Create repostitory](./ECR_Create_Repository_1.png)

Once you created it you can:
- Configure the permissions
- Configure the Lifecycle policy

![Configure Lifecycle](./ECR_Create_Repository_2.png)