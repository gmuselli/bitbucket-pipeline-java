#!/bin/bash

# First configure aws
aws configure set region ${AWS_REGION}
aws configure set output json
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY} 

# Update with latest version (as we may have did a push before)
git pull

# GET the version from the tag
VERSION=$(git describe --abbrev=0 --tags 2> /dev/null)

echo "Version is: ${VERSION}"

# Run command to login to ECR
$(aws ecr get-login --no-include-email --region eu-west-3)

# Tag the image
docker tag ${APPLICATION_NAME}:latest ${AWS_ECR_URL}/${APPLICATION_NAME}:${VERSION}

# Push the image to docker registry
docker push ${AWS_ECR_URL}/${APPLICATION_NAME}:${VERSION}