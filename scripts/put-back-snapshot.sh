#!/bin/bash

# Update with latest version (as we may have did a push before)
git pull

VERSION=$(./mvnw help:evaluate -Dexpression=project.version -q -DforceStdout)
MAJOR=$(echo ${VERSION} | cut -d "." -f 1)
MINOR=$(echo ${VERSION} | cut -d "." -f 2)
./mvnw versions:set -DgenerateBackupPoms=false -DnewVersion=${MAJOR}.${MINOR}-SNAPSHOT

git add --a
git commit -m "[skip ci] Put back SNAPSHOT version"
git push