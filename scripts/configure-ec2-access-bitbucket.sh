#!/bin/bash

function configureAddress() {
	IP=$1
	echo "$1"
	aws ec2 authorize-security-group-ingress \
		--group-name my-sg \
		--ip-permissions IpProtocol=tcp,FromPort=5000,ToPort=5000,IpRanges="[{CidrIp=${IP},Description=\"Bitbucket Pipeline\"}]"
}

configureAddress "34.199.54.113/32"
configureAddress "34.232.25.90/32"
configureAddress "34.232.119.183/32"
configureAddress "34.236.25.177/32"
configureAddress "35.171.175.212/32"
configureAddress "52.54.90.98/32"
configureAddress "52.202.195.162/32"
configureAddress "52.203.14.55/32"
configureAddress "52.204.96.37/32"
configureAddress "34.218.156.209/32"
configureAddress "34.218.168.212/32"
configureAddress "52.41.219.63/32"
configureAddress "35.155.178.254/32"
configureAddress "35.160.177.10/32"
configureAddress "34.216.18.129/32"
