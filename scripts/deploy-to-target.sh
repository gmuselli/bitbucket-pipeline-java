#!/bin/bash

# https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html

aws configure set region ${AWS_REGION}
aws configure set output json
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY}

# Update with latest version (as we may have did a push before)
git pull

# Run command to login to ECR
LOGIN_COMMAND=$(aws ecr get-login --no-include-email --region eu-west-3)
VERSION=$(git describe --abbrev=0 --tags 2> /dev/null)
TARGET=$1
SSH_USER=$2

# first pull the image (in order to not block the current process)
ssh ${SSH_USER}@${TARGET} << EOF
    sudo $LOGIN_COMMAND
    sudo docker pull ${AWS_ECR_URL}/${APPLICATION_NAME}:${VERSION}
EOF
 

# get process and kill it. First connection use StrictHostKeyChecking
CONTAINER=$(ssh ${SSH_USER}@${TARGET} sudo docker ps | grep ${APPLICATION_NAME} | cut -d " " -f 1)

echo "CONTAINER = $CONTAINER"

if [ ! -z "$CONTAINER" ]; then
ssh ${SSH_USER}@${TARGET} << EOF
    sudo docker kill ${CONTAINER} > /dev/null
    echo "Container ${CONTAINER} killed"
    sudo docker rm ${CONTAINER}  > /dev/null
    echo "Container ${CONTAINER} removed"
EOF
fi

ssh ${SSH_USER}@${TARGET} << EOF
    sudo docker run -d -p ${INTERNAL_PORT_TARGET}:${INTERNAL_PORT_TARGET} ${AWS_ECR_URL}/${APPLICATION_NAME}:${VERSION}
EOF

# sleep until it has started
sleep 5

echo "About to run curl -I --silent http://localhost:${INTERNAL_PORT_TARGET}${LIVENESS_PATH} 2> /dev/null | grep OK"

RESULT=$(ssh ${SSH_USER}@${TARGET} curl -I --silent http://localhost:${INTERNAL_PORT_TARGET}${LIVENESS_PATH} 2> /dev/null | grep OK)

if [ -z "${RESULT}" ]; then
    echo "Deployment failed!!!!"
    exit 1
else 
    echo "Internal Call OK"
fi

echo "About to run curl -I --silent "${TARGET}:${INTERNAL_PORT_TARGET}${LIVENESS_PATH}" 2> /dev/null | grep OK"

RESULT=$(curl -I --silent "${TARGET}:${INTERNAL_PORT_TARGET}${LIVENESS_PATH}" 2> /dev/null | grep OK)

if [ -z "${RESULT}" ]; then
    echo "Deployment failed!!!!"
    exit 1
else 
    echo "External Call OK"
fi

